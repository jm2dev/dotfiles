;;; jm -- configuration march 2020
;;; https://nixos.org/nixos/manual/index.html#module-services-emacs

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

(setq package-enable-at-startup nil)
(package-initialize)

;; from graphviz-dot
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; http://pages.sachachua.com/.emacs.d/Sacha.html
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))

(prefer-coding-system 'utf-8)

;; UI
;;(set-default-font "Source Code Pro Light 15")
;;(set-default-font "Hack 15")
;;(set-default-font "Meslo LG L 15")
(set-default-font "Iosevka Term Extended 15")
(tool-bar-mode -1)
(display-time-mode 1)
(line-number-mode 1)
(column-number-mode 1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
;;(toggle-scroll-bar -1)
(global-visual-line-mode 1); Proper line wrapping
(set-fringe-mode '(0 . 0)); Disable fringe because I use visual-line-mode
(show-paren-mode 1) ; turn on paren match highlighting
(setq show-paren-style 'expression) ; highlight entire bracket expression
;; Enable highlighting of current line.
(global-hl-line-mode 1)
;; from elegance emacs
(setq inhibit-startup-screen 1)

; use-package did not work for me in opensuse
(use-package modus-operandi-theme :ensure) 
(use-package modus-vivendi-theme :ensure) 
;(load-theme 'modus-operandi t)          ; Light theme
(load-theme 'modus-vivendi t)           ; Dark theme

;(require 'golden-ratio)
;(golden-ratio-mode 1)

;; package-install ess
;; Polymode for R
(use-package poly-markdown
  :ensure t)
(use-package poly-R
  :ensure t)

(add-to-list 'auto-mode-alist '("\\.md" . poly-markdown-mode))
(add-to-list 'auto-mode-alist '("\\.Rmd" . poly-markdown+r-mode))

;; package-install company
;; Graphviz
(use-package graphviz-dot-mode
  :ensure t
  :config
  (setq graphviz-dot-indent-width 2))

(use-package company-graphviz-dot
  )

;; package-install jinja2-mode
;; package-install yaml-mode
(require 'yaml-mode)
(add-to-list 'auto-mode-alist '("\\.yml" . yaml-mode))

;; package-install terraform-mode

;; https://ogbe.net/emacsconfig.html
(load "server")
(unless (server-running-p) (server-start))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(delete-selection-mode nil)
 '(package-selected-packages
   (quote
    (terraform-mode magit jinja2-mode yaml-mode company ess graphviz-dot-mode poly-R poly-markdown use-package modus-vivendi-theme modus-operandi-theme))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
