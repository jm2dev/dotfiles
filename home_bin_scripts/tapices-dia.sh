#!/usr/bin/env sh

IN="eDP-1"
EXT="DP-1"
#IN="LVDS1"
#EXT="HDMI1"

if (xrandr | grep "${EXT} disconnected"); then
    feh  --bg-fill $HOME/Pictures/wallpapers/wolf-in-sheep-clothes.jpg
else
    feh  --bg-fill $HOME/Pictures/wallpapers/wolf-in-sheep-clothes.jpg $HOME/Pictures/wallpapers/ships.jpg
fi

