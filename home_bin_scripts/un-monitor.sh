#!/usr/bin/env sh

IN="eDP-1"
EXT="DP-1"

xrandr --output $IN --auto --output $EXT --off
feh  --bg-fill $HOME/Pictures/wallpapers/debian-lines.png
