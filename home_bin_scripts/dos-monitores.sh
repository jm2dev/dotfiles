#!/usr/bin/env sh

IN="eDP-1"
EXT="DP-1"
#IN="LVDS1"
#EXT="HDMI1"

if (xrandr | grep "${EXT} disconnected"); then
    xrandr --output $IN --auto $EXT --off
    feh  --bg-fill $HOME/Pictures/wallpapers/wolf-in-sheep-clothes.jpg
else
    xrandr --output $IN --auto --output $EXT --auto --left-of $IN
    feh  --bg-fill $HOME/Pictures/wallpapers/BlackMarble_2016_01deg.jpg $HOME/Pictures/wallpapers/ships.jpg
fi

