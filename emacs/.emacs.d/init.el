;;; jm -- configuration march 2020
;;; https://nixos.org/nixos/manual/index.html#module-services-emacs

(require 'package)

(setq package-enable-at-startup nil)
(package-initialize)

;; http://pages.sachachua.com/.emacs.d/Sacha.html
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))

(prefer-coding-system 'utf-8)

;; UI
(set-default-font "Iosevka Light 19")
(tool-bar-mode -1)
(display-time-mode 1)
(line-number-mode 1)
(column-number-mode 1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
;;(toggle-scroll-bar -1)
(global-visual-line-mode 1); Proper line wrapping
(set-fringe-mode '(0 . 0)); Disable fringe because I use visual-line-mode
(show-paren-mode 1) ; turn on paren match highlighting
(setq show-paren-style 'expression) ; highlight entire bracket expression
;; Enable highlighting of current line.
(global-hl-line-mode 1)

(use-package modus-operandi-theme
  :ensure t) 
(use-package modus-vivendi-theme
  :ensure t) 
(load-theme 'modus-operandi t)          ; Light theme
;(load-theme 'modus-vivendi t)           ; Dark theme

(require 'golden-ratio)
(golden-ratio-mode 1)

;; Nix support
(use-package nix-mode
  :mode "\\.nix\\'")

;; direnv and lorri
(use-package direnv
 :config
 (direnv-mode))

;; Emacs metals
;; Enable scala-mode for highlighting, indentation and motion commands
(use-package scala-mode
  :mode "\\.s\\(cala\\|bt\\|c\\)$")

;; Enable sbt mode for executing sbt commands
(use-package sbt-mode
  :commands sbt-start sbt-command
  :config
  ;; WORKAROUND: https://github.com/ensime/emacs-sbt-mode/issues/31
  ;; allows using SPACE when in the minibuffer
  (substitute-key-definition
   'minibuffer-complete-word
   'self-insert-command
   minibuffer-local-completion-map)
   ;; sbt-supershell kills sbt-mode:  https://github.com/hvesalai/emacs-sbt-mode/issues/152
   (setq sbt:program-options '("-Dsbt.supershell=false"))
)

;; Enable nice rendering of diagnostics like compile errors.
;;(use-package flycheck
;;  :init (global-flycheck-mode))
;; as per https://discourse.nixos.org/t/porting-emacs-config-to-nixos-using-home-manager/5508/2
(use-package flycheck
  :commands global-flycheck-mode
  :init
  (global-flycheck-mode))

(use-package lsp-mode
  ;; Optional - enable lsp-mode automatically in scala files
  :hook  (scala-mode . lsp)
         (lsp-mode . lsp-lens-mode)
  :config (setq lsp-prefer-flymake nil))

;; Enable nice rendering of documentation on hover
(use-package lsp-ui)

;; lsp-mode supports snippets, but in order for them to work you need to use yasnippet
;; If you don't want to use snippets set lsp-enable-snippet to nil in your lsp-mode settings
;;   to avoid odd behavior with snippets and indentation
(use-package yasnippet)

;; Add company-lsp backend for metals
(use-package company-lsp)

;; Use the Debug Adapter Protocol for running tests and debugging
(use-package posframe
  ;; Posframe is a pop-up tool that must be manually installed for dap-mode
  )
(use-package dap-mode
  :hook
  (lsp-mode . dap-mode)
  (lsp-mode . dap-ui-mode)
  )

;; Use the Tree View Protocol for viewing the project structure and triggering compilation
(use-package lsp-treemacs
  :config
  (lsp-metals-treeview-enable t)
  (setq lsp-metals-treeview-show-when-views-received t)
  )

;; Polymode for R
(use-package poly-markdown
  :ensure t)
(use-package poly-R
  :ensure t)

(add-to-list 'auto-mode-alist '("\\.md" . poly-markdown-mode))
(add-to-list 'auto-mode-alist '("\\.Rmd" . poly-markdown+r-mode))

;; html template pug files
(add-to-list 'auto-mode-alist '("\\.pug" . web-mode))

;; Python
;; https://github.com/jorgenschaefer/elpy/issues/1708
(use-package elpy
  :mode ("\\.py\\'" . python-mode)
  :interpreter ("python" . python-mode)
  :init
  (setq-default python-shell-interpreter "python3")
  (setq elpy-shell-use-project-root nil)
  (setq elpy-rpc-python-command "python3")
  (setq elpy-modules (quote
    (elpy-module-company elpy-module-eldoc
    elpy-module-highlight-indentation elpy-module-yasnippet
    elpy-module-autodoc elpy-module-sane-defaults)))
  (elpy-enable))

;; Graphviz
(use-package graphviz-dot-mode
  :ensure t
  :config
  (setq graphviz-dot-indent-width 2))

(use-package company-graphviz-dot
  )

;; https://ogbe.net/emacsconfig.html
(load "server")
(unless (server-running-p) (server-start))

;; nixpkgs
(setq-default indent-tabs-mode nil)

;; https://dougie.io/emacs/indentation/#changing-the-tab-width
;; Our Custom Variable
;;(setq custom-tab-width 2)

;;(setq-default python-indent-offset custom-tab-width)
;;(setq-default evil-shift-width custom-tab-width)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("e2cf69479f6933620a1a214c8e0835dd89577f5b57131aef0302b874ce9ffcac" default)))
 '(package-selected-packages
   (quote
    (graphviz-dot-mode tree-mode terraform-mode scala-mode sbt-mode rjsx-mode poly-R magit lsp-ui golden-ratio go-mode flycheck ess elpy direnv company-lsp company-jedi beacon use-package posframe nix-mode modus-vivendi-theme modus-operandi-theme dap-mode))))
